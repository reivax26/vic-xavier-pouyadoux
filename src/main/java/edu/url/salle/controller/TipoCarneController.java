package edu.url.salle.controller;

import java.util.List;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import ch.qos.logback.classic.Logger;
import edu.url.salle.form.TipoCarneListaForm;
import edu.url.salle.model.TipoCarne;
import edu.url.salle.service.TipoCarneService;

@Controller
@RequestMapping(value = { "/tipocarne" })
public class TipoCarneController {

	@Autowired
	private TipoCarneService tipoCarneService;

	private static final Logger logger = (Logger) LoggerFactory.getLogger(TipoCarneController.class);

	@RequestMapping(method = RequestMethod.GET)
	public final ModelAndView listarTiposCarne() {

		logger.info("Metodo de listar tipos de carne");

		List<TipoCarne> listaTiposCarne = tipoCarneService.listarTiposCarne();

		TipoCarneListaForm tipoCarneListaForm = new TipoCarneListaForm();

		tipoCarneListaForm.setTiposCarne(listaTiposCarne);

		if (!listaTiposCarne.isEmpty()) {
			logger.info(listaTiposCarne.get(0).getNombre());
		}

		return new ModelAndView("tipoCarneLista", "tipoCarneListaForm", tipoCarneListaForm);

	}

}