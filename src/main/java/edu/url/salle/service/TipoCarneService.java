package edu.url.salle.service;

import java.util.List;

import edu.url.salle.model.TipoCarne;

public interface TipoCarneService {

	public List<TipoCarne> listarTiposCarne();
}
