DROP TABLE IF EXISTS tipo_carne;

CREATE TABLE tipo_carne(
	id INT AUTO_INCREMENT PRIMARY KEY,
	nombre VARCHAR(250) NOT NULL,
	comentario VARCHAR(250) NOT NULL
);

INSERT INTO tipo_carne(nombre, comentario) VALUES
	('Ternera','De nuestra granjas'),
	('Pollo','Criados en comidad'),
	('Cerdo','Hasta los andares se come');